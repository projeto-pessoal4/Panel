﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Painel.aspx.cs" Inherits="ProjetoPainel.Painel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="panelStyleSheet.css" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Painel de Cadastro</title>
</head>
<body>
    <div class="form-container">
        <form id="form1" runat="server">
            <table class="tabela1">
                <tr>
                    <td class="td-titulo"><span>Painel de Cadastro</span></td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="painel1" runat="server" Visible="true">
                            <asp:Panel ID="painel2" runat="server" Visible="true">
                                <table>
                                    <tr>
                                        <td class="td-subtitulo">Informações Pessoais</td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <label for="txtNome">Nome:</label>
                                             <asp:TextBox ID="txtNome" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <label for="txtSobreNome">Sobrenome:</label>
                                             <asp:TextBox ID="txtSobreNome" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <label for="txtGenero">Genero:</label>
                                             <asp:TextBox ID="txtGenero" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <label for="txtCelular">Celular:</label>
                                             <asp:TextBox ID="txtCelular" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="proximo1" runat="server" Text="Proximo" OnClick="btnProximo1_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="painel3" runat="server" Visible="false">
                                <table>
                                    <tr>
                                        <td class="td-subtitulo">Detalhes do Endereço</td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <label for="txtEndereco">Endereço:</label>
                                             <asp:TextBox ID="txtEndereco" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <label for="txtCidade">Cidade:</label>
                                             <asp:TextBox ID="txtCidade" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <label for="txtCep">CEP:</label>
                                             <asp:TextBox ID="txtCep" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="voltar1" runat="server" Text="Voltar" OnClick="btnVoltar1_Click" />
                                            <asp:Button ID="proximo2" runat="server" Text="Proximo" OnClick="btnProximo2_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="painel4" runat="server" Visible="false">
                                <table>
                                    <tr>
                                        <td class="td-subtitulo">Área de Login</td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <label for="txtUsuario">Usuário:</label>
                                             <asp:TextBox ID="txtUsuario" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <label for="txtSenha">Senha:</label>
                                             <asp:TextBox ID="txtSenha" runat="server" TextMode="Password"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="voltar2" runat="server" Text="Voltar" OnClick="btnVoltar2_Click" />
                                            <asp:Button ID="enviar" runat="server" Text="Enviar" OnClick="btnEnviar_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="envioDosDados" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</body>
</html>
