﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjetoPainel
{
    public partial class Painel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnProximo1_Click(object sender, EventArgs e) {
            painel2.Visible = false;
            painel3.Visible = true;
            painel4.Visible = false;
        }

        protected void btnProximo2_Click(object sender, EventArgs e) {
            envioDosDados.Text = "";
            painel2.Visible = false;
            painel3.Visible = false;
            painel4.Visible = true;
        }

        protected void btnVoltar1_Click(object sender, EventArgs e) {
            painel2.Visible = true;
            painel3.Visible = false;
            painel4.Visible = false;
        }

        protected void btnVoltar2_Click(object sender, EventArgs e) {
            painel2.Visible = false;
            painel3.Visible = true;
            painel4.Visible = false;
        }

        protected void btnEnviar_Click(object sender, EventArgs e) {
            if(!string.IsNullOrEmpty(txtUsuario.Text) && !string.IsNullOrEmpty(txtSenha.Text)) {
                envioDosDados.Text = "Dados Enviados com Sucesso";
            } else {
                envioDosDados.Text = "Preencha todos os campos para enviar";
            }
            
        }

    }
}